function callBack(err, { id, fetchedData: board, cb }) {
    if (err != null) {
        cb(err);
    }
    else {
        setTimeout(() => {
            try {
                let batch = board.find(elem => elem.id == id);
                let foundError = batch ? null : `${id} is not available in Board.json`;
                if (foundError != null) {
                    batch = board.find(elem => elem.name == id);
                    foundError = batch ? null : `${id} is not available in Board.json`;
                }
                cb(foundError, batch);

            }
            catch (error) {
                if (typeof cb != 'function') {
                    console.log('Error: cb is not a function', error.stack);
                }
                else {
                    cb(error);
                }
            }
        }, 2 * 1000);
    }
}

module.exports = callBack;