const callList = require('./callBack2');
const callCards = require('./callBack3');
const callBoard = require('./callBack1');

const path = require('path');

const reader = require('./test/jsonToArray');

const pathOfList = path.join(__dirname + '/data/lists.json');
const pathOfCards = path.join(__dirname + '/data/cards.json');



function testCallBack(err, { fetchedData: board }) {
    if (err != null) {
        console.log(err);
    }
    else {
        const name = 'Thanos';
        callBoard(null, { id: name, fetchedData: board, cb: getID });
    }
}

function getID(err, batch) {
    if (err) {
        console.log("ERROR", err);
    }
    else {
        console.log(`${batch.name} ID: `, batch.id);
        reader({ path: pathOfList, cb: callList, id: batch.id, cb2: getListInfo });
    }
}

function getListInfo(err, list) {
    if (err) {
        console.log("ERROR", err);
    }
    else {
        list.forEach(elem => {
            console.log(`${elem.name} ID: `, elem.id);
            reader({ path: pathOfCards, cb: callCards, id: elem.id, cb2: getCardsInfo });
        });
    }
}


function getCardsInfo(err, data) {
    if (err) {
        console.log("ERROR", err);
        return;
    }
    console.log(data);
}

module.exports = testCallBack;