function callBack(err, { fetchedData: cards, id, cb }) {
    if (err != null) {
        cb(err);
    }
    else {
        setTimeout(() => {
            try {
                if (Array.isArray(cards)) {
                    cb('Cards.json is an array')
                }
                else if (cards[id] == undefined) {
                    cb(new Error(`${id} is not found in Cards.json`).message);
                }
                else {
                    cb(null, cards[id]);
                }
            } catch (error) {
                if (typeof cb != 'function') {
                    console.log('Error: cb is not a function', error.stack);
                }
                else {
                    cb(error);
                }
            }
        }, 2 * 1000);

    }
}

module.exports = callBack;