/* 
    Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const path = require('path');
const pathOfBoards = path.join(__dirname + '/../data/boards.json');
const reader = require('./jsonToArray');
const callBack6 = require('../callBack6');


reader({ path: pathOfBoards, cb: callBack6 });