const fs = require('fs');

function reader({ path, cb, id, cb2 }) {
    if (typeof cb !== 'function') {
        console.log('Error: cb is not a function', error.stack);
    }
    else if (typeof path != 'string') {
        cb(new Error('Path is undefined'), { cb: cb2 })
    }
    else {
        fs.readFile(path, 'utf-8', (err, msg) => {
            if (err) {
                cb(err.message, { cb: cb2 });
            }
            else {
                try {
                    const jsonData = JSON.parse(msg);
                    cb(null, { fetchedData: jsonData, id, cb: cb2 });
                } catch (error) {
                    cb(error.message, { cb: cb2 })
                }
            }
        });
    }

}

module.exports = reader;