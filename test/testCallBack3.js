/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/
const path = require('path');

const reader = require('./jsonToArray');
const callBack3 = require('../callBack3');

const pathOfCards = path.join(__dirname + '/../data/cards.json');

reader({ path: pathOfCards, cb: testCallBack });

const listsID = "jwkh245"
function testCallBack(err, { fetchedData: cards }) {
	if (err != null) {
		console.log('ERROR:', err);
	}
	else {
		setTimeout(() => {
			callBack3(null, { id: listsID, fetchedData: cards, cb: printer });
		}, 2 * 1000);

	}
}


function printer(err, details) {
	if (err != null) {
		console.log("ERROR:", err);
	}
	else {
		console.log(details);
	}
}