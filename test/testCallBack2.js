/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/
const path = require('path');
const reader = require('./jsonToArray');
const callBack2 = require('../callBack2');

const pathOfLists = path.join(__dirname + '/../data/lists.json');
const boardID = "mcu453ed";

reader({ path: pathOfLists, cb: testcallback });

function testcallback(err, { fetchedData: list }) {
	if (err != null) {
		console.log("ERROR:", err);
	}
	else {
		setTimeout(() => {
			callBack2(null, { fetchedData: list, id: boardID, cb: printer });
		}, 2 * 1000);

	}
}

function printer(err, details) {
	if (err != null) {
		console.log("ERROR:", err);
	}
	else {
		console.log(details);
	}
}
