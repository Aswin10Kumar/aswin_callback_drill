/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

const reader = require('./jsonToArray');
const callBack1 = require('../callBack1');
const path = require('path');

const boardID = "mcu453ed"
const pathOfBoards = path.join(__dirname + '/../data/boards.json');

reader({ path: pathOfBoards, cb: testCallBack });


function testCallBack(err, { fetchedData: board }) {
	if (err !== null) {
		console.log("ERROR:", err)
	}
	else {
		setTimeout(() => {
			callBack1(null, { id: boardID, fetchedData: board, cb: printer });
		}, 2 * 1000);
	}
}

function printer(err, details) {
	if (err != null) {
		console.log("ERROR", err);
	}
	else {
		console.log(details);
	}
}