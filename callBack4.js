const callBoard = require('./callBack1');
const callList = require('./callBack2');
const callCards = require('./callBack3');
const path = require('path');

const reader = require('./test/jsonToArray');

const pathOfList = path.join(__dirname + '/data/lists.json');
const pathOfCards = path.join(__dirname + '/data/cards.json');

function callBack(err, { fetchedData: board }) {
    const name = 'Thanos';
    callBoard(null, { id: name, fetchedData: board, cb: getID });
}

function getID(err, batch) {
    if (err) {
        console.log("ERROR", err);
    }
    else {
        console.log(`${batch.name} ID: `, batch.id);
        reader({ path: pathOfList, cb: callList, id: batch.id, cb2: getListInfo });
    }
}

function getListInfo(err, list) {
    if (err != null) {
        console.log("ERROR:", err);
    }
    else {
        const name = 'Mind';
        const batch = list.find(elem => elem.name == name);
        let foundError = batch ? null : `${name} is not available in List.json`;
        if (foundError == null) {
            console.log(`${name} ID: `, batch.id);
            reader({ path: pathOfCards, cb: callCards, id: batch.id, cb2: getCardsInfo });
        } else {
            console.log(`${name} is not available in List.json`);
        }
    }
}

function getCardsInfo(err, data) {
    if (err != null) {
        console.log("ERROR:", err);
    }
    else {
        console.log(data);
    }
}

module.exports = callBack;