function list(err, { fetchedData: list, id, cb }) {
    if (err != null) {
        cb(err);
    }
    else {

        setTimeout(() => {
            try {
                if (Array.isArray(list)) {
                    cb('List.json is an Array');
                }
                else if (list[id] == undefined) {
                    cb(`${id} is not found in List.json`);
                }
                else {
                    cb(null, list[id]);
                }
            } catch (error) {
                if (typeof cb != 'function') {
                    console.log('Error: cb is not a function', error.stack);
                }
                else {
                    cb(error);
                }
            }
        }, 2 * 1000);

    }
}

module.exports = list;